<?php

require '../app/start.php';

$app->run();

//TODO:
//1. js: changing opacity of form when hovering -> darker, when off -> lighter,
//so that visibility compared to the map changes 
//2. taking points locations from map markers
//3. if points are written, show them on map
//4. change display result m/km
//5. changing N/S W/E on switch button
//6. rolling the form to some little button so that the whole map would be visible
