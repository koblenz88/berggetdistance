$(document).ready(function(){
    document.getElementById("output").value = "";
    $('[data-toggle="popover"]').popover({
        container: 'body'
    });   
});
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$('#mySubmitButton').on('click', function () {
    //var $btn = $(this).button('loading')
    // business logic...
    var fields = {NS_first:"NSfirstquery", WE_first:"WEfirstquery", NS_second:"NSsecondquery", WE_second:"WEsecondquery"}
    var f
    var NS_pattern = /^(N|S) \d{2}, \d{2}, \d{2}\.\d{3}$/;
    var WE_pattern = /^(W|E) \d{2,3}, \d{2}, \d{2}\.\d{3}$/;
    
    var values = []
    
    for (f in fields) {
        var x = document.getElementById(fields[f]).value;
        
        if (x == null || x == "") {
            alert("Coordinates " + f + " must be filled out");
            return false;
        }
        
        var mode = f.split("_")[0];
        var v = mode == 'NS' ? NS_pattern : WE_pattern;
        if (!v.test(x)) {
            alert("Wrong format in " + f + ". Please improve.");
            return false;
        }
        values[f] = x;//.push(x);
    }
    
    var xmlhttp;
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            var resp = JSON.parse(xmlhttp.responseText);
            document.getElementById("output").value = resp.km;//.km
            //document.getElementById("output").value=xmlhttp.responseText;
        }
    }
    
    xmlhttp.open("GET", "/geoform?ns1="+values["NS_first"]+"&we1="+values["WE_first"]+"&ns2="+values["NS_second"]+"&we2="+values["WE_second"], true);
    xmlhttp.send();
    

    
    //$btn.button('reset')
  })
  
//function searchLocations() {
//  var address = document.getElementById("addressInput").value;
//  var geocoder = new google.maps.Geocoder();
//  geocoder.geocode({address: address}, function(results, status) {
//    if (status == google.maps.GeocoderStatus.OK) {
//      searchLocationsNear(results[0].geometry.location);
//    } else {
//      alert(address + ' not found');
//    }
//  });
//}

//var geocoder = new google.maps.Geocoder();
//var map;
//var infowindow = new google.maps.InfoWindow();
//var marker;
//function initialize() {
//  map = new google.maps.Map(document.getElementById('map-canvas'), {
//        zoom: 8,
//        center: {lat: 40.730885, lng: -73.997383}
//      });
//}

//function codeLatLng() {
//  var input = document.getElementById('latlng').value;
//  var latlngStr = input.split(',', 2);
//  var latlng = new google.maps.LatLng(latlngStr[0], latlngStr[1]);
//  geocoder.geocode({'location': latlng}, function(results, status) {
//    if (status == google.maps.GeocoderStatus.OK) {
//      if (results[1]) {
//        map.setZoom(11);
//        marker = new google.maps.Marker({
//          position: latlng,
//          map: map
//        });
//        infowindow.setContent(results[1].formatted_address);
//        infowindow.open(map, marker);
//      } else {
//        window.alert('No results found');
//      }
//    } else {
//      window.alert('Geocoder failed due to: ' + status);
//    }
//  });
//}

//google.maps.event.addDomListener(window, 'load', initialize);
