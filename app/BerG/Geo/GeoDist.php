<?php

namespace BerG\Geo;

class GeoDist
{
    private $request = null;
    private $response = null;

    public function __construct($ns1, $we1, $ns2, $we2)
    {
        $this->request = [
            "ns1" => trim($ns1),
            "we1" => trim($we1),
            "ns2" => trim($ns2),
            "we2" => trim($we2),
        ];
        $this->response = [
            "km" => 0,
            "m" => 0,// * 1000,
            "errid" => 0,
            "errmsg" => "",
        ];
    }

    public function validate()
    {
        $pattern_ns = "/^(N|S) \d{1,2}, \d{2}, \d{2}\.\d{3}$/";
        $pattern_we = "/^(W|E) \d{1,2,3}, \d{2}, \d{2}\.\d{3}$/";
        foreach ($this->request as $subject)
        {
            $sub = substr($subject, 0, 1);

            if ($sub === 'N' or $sub === 'S')
            {
                $pattern = $pattern_ns;
            }
            elseif ($sub === 'W' or $sub === 'E')
            {
                $pattern = $pattern_we;
            }
            else
            {
                $this->setErrorMessage(1, "Wrong type.");
                return false;
            }

            if (!preg_match($pattern, $subject))
            {
                $this->setErrorMessage(1, "Regexp no match.");
                return false;
            }
            return true;
        }
    }

    public function prepare()
    {
        foreach ($this->request as &$v)
        {
            $arr = explode(" ", $v);
            foreach ($arr as &$elem)
            {
                strtr($elem, array('.' => '', ',' => '', ' ' => ''));
            }
            unset($elem);
            $v = $arr[1] . "." . $arr[2] . $arr[3];

            if ($arr[0] === 'S' or $arr[0] === 'W')
            {
                $v = -1 * (float)$v;
            }
        }
        unset($v);
        return $this;
    }
    public function calcDistance()
    {
        //32.9697, -96.80322, 29.46786, -98.53506

        $this->addResponseDistance(
            $this->vincentyGreatCircleDistance(
                (double)$this->request["ns1"],
                (double)$this->request["we1"],
                (double)$this->request["ns2"],
                (double)$this->request["we2"]
            )
        );
    }
    private function addResponseDistance($disr_km)
    {
        $this->response["km"] = $disr_km;
        $this->response["m"] = $disr_km * 1000;
    }
    public function getResponse()
    {
        return $this->response;
    }

    public function setErrorMessage($error_id, $error_mssg)
    {
        $this->response["errid"] = $error_id;
        $this->response["errmsg"] = $error_mssg;
    }

    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::                                                                         :*/
    /*::  This routine calculates the distance between two points (given the     :*/
    /*::  latitude/longitude of those points). It is being used to calculate     :*/
    /*::  the distance between two locations using GeoDataSource(TM) Products    :*/
    /*::                                                                         :*/
    /*::  Definitions:                                                           :*/
    /*::    South latitudes are negative, east longitudes are positive           :*/
    /*::                                                                         :*/
    /*::  Passed to function:                                                    :*/
    /*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
    /*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
    /*::    unit = the unit you desire for results                               :*/
    /*::           where: 'M' is statute miles (default)                         :*/
    /*::                  'K' is kilometers                                      :*/
    /*::                  'N' is nautical miles                                  :*/
    /*::  Worldwide cities and other features databases with latitude longitude  :*/
    /*::  are available at http://www.geodatasource.com                          :*/
    /*::                                                                         :*/
    /*::  For enquiries, please contact sales@geodatasource.com                  :*/
    /*::                                                                         :*/
    /*::  Official Web site: http://www.geodatasource.com                        :*/
    /*::                                                                         :*/
    /*::         GeoDataSource.com (C) All Rights Reserved 2015		   		     :*/
    /*::                                                                         :*/
    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K') {

      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +
              cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
          }
    }


    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    private function haversineGreatCircleDistance(
      $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
      // convert from degrees to radians
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);

      $latDelta = $latTo - $latFrom;
      $lonDelta = $lonTo - $lonFrom;

      $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
      return $angle * $earthRadius;
    }


    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    private function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6372.795)//6371000
    {
      // convert from degrees to radians
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);

      $lonDelta = $lonTo - $lonFrom;
      $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
      $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

      $angle = atan2(sqrt($a), $b);
      return $angle * $earthRadius;
    }
}
