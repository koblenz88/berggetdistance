<?php

use BerG\Geo\GeoDist as GDist;

$app->get('/geoform', function() {

    $geo = new GDist($_GET["ns1"], $_GET["we1"], $_GET["ns2"], $_GET["we2"]);
//echo 'ble';
    if ($geo->validate())
    {
        $geo->prepare()->calcDistance();
    }

    echo json_encode($geo->getResponse());
})->name('geoform');

//$app->get('/geoform', function() use ($app) {
//
//    $app->render('geoform.html.twig');
//    //$app->redirect('/');
//
//})->name('get_geoform');


